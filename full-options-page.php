<?php
/*
Template Name: Page toutes options
*/
 ?>


 <?php get_header(); ?>

  <div class="row">
    <main id="main-home" <?php if(isset($_POST['nom']) || isset($_POST['mail']) || isset($_POST['message']) ){ echo 'class="contact-open"';}
    // Si il y a un post, c'est que le formulaire a été envoyé, on ajoute un class pour ouvrir le block contact via le script?>>

    <?php

    // Puis on remplit le contenu de la page


    if( have_rows('contenu_de_la_page') ):

      while ( have_rows('contenu_de_la_page') ) : the_row();

                if(have_rows('param_bloc')):
                  while(have_rows('param_bloc')): the_row();
                  //variables :
                  $have_titre = false;
                  // Titre du bloc
                  $titre_du_bloc = get_sub_field('parametres_titre_bloc');
                  $ajouter_titre_bloc = $titre_du_bloc['ajouter_titre_bloc'];

                  if($ajouter_titre_bloc):
                    $have_titre = true;
                    // Récupération des paramètres
                    $options_titre = $titre_du_bloc['options_titre'];
                    $intitule_titre_bloc = $titre_du_bloc['intitule'];
                    $police = $titre_du_bloc['option_police'];
                    $alignement = $titre_du_bloc['alignement'];

                    if($options_titre && in_array('uppercase', $options_titre)):
                      $uppercase = true;
                    else:
                      $uppercase = false;
                    endif;
                    if($options_titre && in_array('underline', $options_titre)):
                      $underline = true;
                    else : $underline = false;
                    endif;
                    if($options_titre && in_array('color_titre', $options_titre)):
                      $couleur_titre_bloc_modif = true;
                      $couleur_titre_bloc = $titre_du_bloc['couleurs_du_titre'];
                      $couleur_label_titre_bloc = $couleur_titre_bloc['couleur_du_titre'];
                      $couleur_fond_titre_bloc = $couleur_titre_bloc['couleur_du_fond'];
                    else : $couleur_titre_bloc_modif = false;
                    endif;

                    //instanciation de la div titre avant le bloc
                    ?>
                    <div class="titre-bloc-container" <?php if($couleur_titre_bloc_modif): echo 'style="color : '.$couleur_label_titre_bloc.'; background-color : '.$couleur_fond_titre_bloc.'"'; endif; ?>>
                      <div class="titre-bloc with-marge" >
                        <h2 class="police-<?php echo $police;?> justify-<?php echo $alignement; if($uppercase):echo ' to-upper'; endif; if($underline): echo ' underline'; endif;?>"
                            >
                          <?php echo $intitule_titre_bloc; ?>
                        </h2>
                      </div>
                    </div>

                    <?php
                  endif; //$ajouter_titre_bloc

                  //Fond du bloc
                  $type_de_fond = get_sub_field('type_de_fond');
                  $fond_color = false;
                  $fond_image = false;

                  switch ($type_de_fond):
                    case 'color':
                      $fond_color = true;
                      $couleur_fond_bloc = get_sub_field('couleur_de_fond');
                      break;

                    case 'image':
                      $fond_image = true;
                      $image_fond_bloc = get_sub_field('image_de_fond');
                      if($image_fond_bloc['sizes']['large']):
                        $image_fond_bloc = $image_fond_bloc['sizes']['large'];
                      else:
                        $image_fond_bloc = $image_fond_bloc['url'];
                      endif;

                      break;

                    default:
                      break;
                  endswitch;
                  // On instancie la div d'article:
                  ?>
                  <div class="bloc-container"
                  <?php if($fond_color): echo 'style="background-color : '.$couleur_fond_bloc.';"';
                        elseif($fond_image): echo 'style="background : url('.$image_fond_bloc.') center center;
                        -webkit-background-size: cover;
                        -moz-background-size: cover;
                        -o-background-size: cover;
                        background-size: cover;"';
                        endif; ?>
                  >


                  <div class="bloc with-marge">

                  <?php

                    // CONTENU DE L'ARTICLE
                    if( have_rows('contenu_article')):
                      $nb_article = 0;
                      while( have_rows('contenu_article')): the_row();

    //----------------------BLOC LARGEUR COMPLETE PARAGRAPHE -----------------------
                        if(get_row_layout() == 'paragraphe_full'):
                          $paragraphe = get_sub_field('paragraphe');

                          if($paragraphe):
                            $couleur_paragraphe = $paragraphe['couleurs_du_bloc'];
                            $titre_paragraphe = $paragraphe['parametres_titre'];
                            $contenu_paragraphe = $paragraphe['texte_paragraphe'];

                            $change_color = $couleur_paragraphe['modifier_couleurs_bloc'];
                            $fond_para_modifier = false;
                            $titre_para_modifier = false;
                            $texte_para_modifier = false;

                            if($change_color):

                              $couleur_a_modifier = $couleur_paragraphe['couleurs_a_modifier'];
                              if($couleur_a_modifier && in_array('fond', $couleur_a_modifier)):
                                $fond_para_modifier = true;
                                $couleur_fond_para_modifier = $couleur_paragraphe['couleur_du_fond'];
                              endif;

                              if($couleur_a_modifier && in_array('titre', $couleur_a_modifier)):
                                $titre_para_modifier = true;
                                $couleur_titre_para_modifier = $couleur_paragraphe['couleur_du_titre'];
                              endif;

                              if($couleur_a_modifier && in_array('texte', $couleur_a_modifier)):
                                $texte_para_modifier = true;
                                $couleur_texte_para_modifier = $couleur_paragraphe['couleur_du_texte'];
                              endif;
                            endif; // $change_color

                            //On instancie la div paragraphe
                            ?>
                            <div class="article-paragraphe article <?php if($nb_article % 2 == 0): echo 'pair'; else : echo 'impair'; endif;?>"
                              <?php if($fond_para_modifier): echo 'style="background-color : '.$couleur_fond_para_modifier.';"'; endif; ?>
                            >
                            <?php

                            $ajouter_titre_paragraphe = $titre_paragraphe['ajouter_titre_paragraphe'];

                            if($ajouter_titre_paragraphe):
                              // print_r($titre_paragraphe);

                              $options_titre = $titre_paragraphe['options_titre'];
                              $intitule_titre_para = $titre_paragraphe['intitule'];
                              $police = $titre_paragraphe['option_police'];
                              $alignement = $titre_paragraphe['alignement'];

                              if($options_titre && in_array('uppercase', $options_titre)):
                                $uppercase = true;
                              else:
                                $uppercase = false;
                              endif;
                              if($options_titre && in_array('underline', $options_titre)):
                                $underline = true;
                              else : $underline = false;
                              endif;

                              //H2 si pas de titre de bloc, h3 si titre au bloc
                              if($have_titre):
                                ?>
                                <h3 class="police-<?php echo $police;?> justify-<?php echo $alignement; if($uppercase):echo ' to-upper'; endif; if($underline): echo ' underline'; endif;?>"
                                    <?php if($titre_para_modifier): echo 'style="color : '.$couleur_titre_para_modifier.';"'; endif; ?> >
                                  <?php echo $intitule_titre_para; ?>
                                </h3>
                                <?php
                              else:
                                ?>
                                <h2 class="police-<?php echo $police;?> justify-<?php echo $alignement; if($uppercase):echo ' to-upper'; endif; if($underline): echo ' underline'; endif;?>"
                                    <?php if($titre_para_modifier): echo 'style="color : '.$couleur_titre_para_modifier.';"'; endif; ?> >
                                  <?php echo $intitule_titre_para; ?>
                                </h2>
                                <?php
                              endif; // ajouter_titre_bloc
                            endif; // $ajouter_titre_paragraphe

                            //Contenu du paragraphe :
                            ?>
                            <div class="paragraphe-content"
                              <?php if($texte_para_modifier): echo 'style="color : '.$couleur_texte_para_modifier.'"'; endif; ?>
                            >
                            <?php echo $contenu_paragraphe; ?>
                            </div>
                          </div>
                            <?php
                          endif;// $paragraphe

    //-------------------- BLOC LARGEUR COMPLETE IMAGE -----------------------------
                        elseif(get_row_layout() == 'image'):
                          $nb_article --; // Pour garder la bonne dispostion
                          $image = get_sub_field('image');

                          $imgUrl = $image['image'];
                          $maxWidthMobile = $image['largeur_maximale_mobile'];
                          $maxWidthDesktop = $image['largeur_maximale_desktop'];
                          $alignement = $image['alignement'];

                          ?>
                            <div class="article-image justify-<?php echo $alignement; ?>">
                              <img class="img-mobile-<?php echo $maxWidthMobile; ?> img-desktop-<?php echo $maxWidthDesktop; ?>"
                                    src="<?php echo $imgUrl['url']; ?>"
                                    alt="<?php echo $imgUrl['alt']; ?>"
                                    <?php if($image['limit_height']): if($image['height']): $height = $image['height']; echo 'style="max-height : '.$height.'px"'; endif;endif; ?> >
                            </div>
                          <?php

    //-------------------- BLOC LARGEUR COMPLETE TEXTE / IMAGE -----------------------------
                        elseif(get_row_layout() == 'bloc_texte_image'):
                          $image_info = get_sub_field('image');
                          $paragraphe = get_sub_field('paragraphe');
                          $agencement = get_sub_field('agencement');
                          $titre_bloc = get_sub_field('parametres_titre');
                          $couleurs_texte_image = get_sub_field('couleurs_du_bloc');

                          $ajouter_titre_bloc_texte_image = $titre_bloc['ajouter_titre_paragraphe'];

                          $change_color = $couleurs_texte_image['modifier_couleurs_bloc'];
                          $fond_texte_image_modifier = false;
                          $titre_texte_image_modifier = false;
                          $texte_texte_image_modifier = false;

                          if($change_color):

                            $couleur_a_modifier = $couleurs_texte_image['couleurs_a_modifier'];
                            if($couleur_a_modifier && in_array('fond', $couleur_a_modifier)):
                              $fond_texte_image_modifier = true;
                              $couleur_fond_texte_image_modifier = $couleurs_texte_image['couleur_du_fond'];
                            endif;

                            if($couleur_a_modifier && in_array('titre', $couleur_a_modifier)):
                              $titre_texte_image_modifier = true;
                              $couleur_titre_texte_image_modifier = $couleurs_texte_image['couleur_du_titre'];
                            endif;

                            if($couleur_a_modifier && in_array('texte', $couleur_a_modifier)):
                              $texte_texte_image_modifier = true;
                              $couleur_texte_texte_image_modifier = $couleurs_texte_image['couleur_du_texte'];
                            endif;
                          endif; // $change_color

                          //On instancie la div texte-image
                          ?>
                          <div class="article-texte-image article <?php if($nb_article % 2 == 0): echo 'pair'; else : echo 'impair'; endif;?>"
                            <?php if($fond_texte_image_modifier): echo 'style="background-color : '.$couleur_fond_texte_image_modifier.';"'; endif; ?>
                          >
                          <?php

                          if($ajouter_titre_bloc_texte_image):

                            $options_titre = $titre_bloc['options_titre'];
                            $intitule_titre = $titre_bloc['intitule'];
                            $police = $titre_bloc['option_police'];
                            $alignement = $titre_bloc['alignement'];

                            if($options_titre && in_array('uppercase', $options_titre)):
                              $uppercase = true;
                            else:
                              $uppercase = false;
                            endif;
                            if($options_titre && in_array('underline', $options_titre)):
                              $underline = true;
                            else : $underline = false;
                            endif;

                            //H2 si pas de titre de bloc, h3 si titre au bloc
                            if($have_titre):
                              ?>
                              <h3 class="police-<?php echo $police;?> justify-<?php echo $alignement; if($uppercase):echo ' to-upper'; endif; if($underline): echo ' underline'; endif;?>"
                                  <?php if($titre_texte_image_modifier): echo 'style="color : '.$couleur_titre_texte_image_modifier.';"'; endif; ?> >
                                <?php echo $intitule_titre; ?>
                              </h3>
                              <?php
                            else:
                              ?>
                              <h2 class="police-<?php echo $police;?> justify-<?php echo $alignement; if($uppercase):echo ' to-upper'; endif; if($underline): echo ' underline'; endif;?>"
                                  <?php if($titre_texte_image_modifier): echo 'style="color : '.$couleur_titre_texte_image_modifier.';"'; endif; ?> >
                                <?php echo $intitule_titre; ?>
                              </h2>
                              <?php
                            endif; // ajouter_titre_bloc
                          endif; // $ajouter_titre_paragraphe

                          // On instancie la div qui va contenir et le texte et l'image :

                          ?>
                          <div class="texte-image-container <?php echo $agencement; ?>">
                            <?php
                              //On récupère l'image:
                                  $image_bloc = $image_info['image_bloc'];
                                  $img = $image_bloc['image'];
                                  if($img['sizes']['large']):
                                    $imgUrl = $img['sizes']['large'];
                                  else:
                                    $imgUrl = $img['url'];
                                  endif;

                                  $largeur_image = $image_info['largeur_de_limage'];
                                  // print_r($image_bloc);
                                  ?>
                                  <div class="image-container desktop-width-<?php echo $largeur_image; ?>" >
                                    <img  src="<?php echo $imgUrl; ?>"
                                          alt="<?php echo $img['alt']; ?>"
                                          <?php if($image_bloc['limit_height']): if($image_bloc['height']): $height = $image_bloc['height']; echo 'style="max-height : '.$height.'px"'; endif;endif; ?> >
                                  </div>


                            <div class="texte-container desktop-width-<?php echo 100-$largeur_image; ?>">
                                <div class="paragraphe-content"
                                <?php if($texte_texte_image_modifier): echo 'style="color : '.$couleur_texte_texte_image_modifier.';"'; endif;?>
                                >
                                <?php
                                echo $paragraphe;
                                ?>
                                </div>

                            </div>
                          </div>
                          <!-- End texte-image-container -->
                        </div>
                        <!-- End sub-article texte-image -->
                          <?php

    //-------------------- BLOC LARGEUR COMPLETE BLOC DEUX COLONNES -----------------------------
                        elseif(get_row_layout() == 'sous_bloc_sur_deux_colonnes'):
                          if(have_rows('contenu')):
                            $nb_article --; // Pour garder la bonne dispostion

                            //On instancie la div container-deux-colonne
                            ?>
                            <div class="bloc-deux-colonnes">
                            <?php

                            $nbContenu = 0;
                              while(have_rows('contenu')): the_row();
                              //On instancie la div sub-article
                              ?>
                              <div class="sub-bloc-colonnes <?php if(($nbContenu % 2) == 1): echo 'sub-col-right impair'; else: echo 'sub-col-left pair'; endif;?>"
                              >
                              <?php

                                if(get_row_layout() == 'paragraphe'):

                                  $paragraphe = get_sub_field('paragraphe');
                                  if($paragraphe):

                                    $couleur_paragraphe = $paragraphe['couleurs_du_bloc'];
                                    $titre_paragraphe = $paragraphe['parametres_titre'];
                                    $contenu_paragraphe = $paragraphe['texte_paragraphe'];

                                    $change_color = $couleur_paragraphe['modifier_couleurs_bloc'];
                                    $fond_para_modifier = false;
                                    $titre_para_modifier = false;
                                    $texte_para_modifier = false;

                                    if($change_color):

                                      $couleur_a_modifier = $couleur_paragraphe['couleurs_a_modifier'];
                                      if($couleur_a_modifier && in_array('fond', $couleur_a_modifier)):
                                        $fond_para_modifier = true;
                                        $couleur_fond_para_modifier = $couleur_paragraphe['couleur_du_fond'];
                                      endif;

                                      if($couleur_a_modifier && in_array('titre', $couleur_a_modifier)):
                                        $titre_para_modifier = true;
                                        $couleur_titre_para_modifier = $couleur_paragraphe['couleur_du_titre'];
                                      endif;

                                      if($couleur_a_modifier && in_array('texte', $couleur_a_modifier)):
                                        $texte_para_modifier = true;
                                        $couleur_texte_para_modifier = $couleur_paragraphe['couleur_du_texte'];
                                      endif;
                                    endif; // $change_color

                                    ?>
                                    <div class="paragraphe-container article" <?php if($fond_para_modifier): echo 'style="background-color : '.$couleur_fond_para_modifier.';"'; endif; ?>>

                                    <?php
                                    $ajouter_titre_paragraphe = $titre_paragraphe['ajouter_titre_paragraphe'];

                                    if($ajouter_titre_paragraphe):
                                      // print_r($titre_paragraphe);

                                      $options_titre = $titre_paragraphe['options_titre'];
                                      $intitule_titre_para = $titre_paragraphe['intitule'];
                                      $police = $titre_paragraphe['option_police'];
                                      $alignement = $titre_paragraphe['alignement'];

                                      if($options_titre && in_array('uppercase', $options_titre)):
                                        $uppercase = true;
                                      else:
                                        $uppercase = false;
                                      endif;
                                      if($options_titre && in_array('underline', $options_titre)):
                                        $underline = true;
                                      else : $underline = false;
                                      endif;

                                      //H2 si pas de titre de bloc, h3 si titre au bloc
                                      if($have_titre):
                                        ?>
                                        <h3 class="police-<?php echo $police;?> justify-<?php echo $alignement; if($uppercase):echo ' to-upper'; endif; if($underline): echo ' underline'; endif;?>"
                                            <?php if($titre_para_modifier): echo 'style="color : '.$couleur_titre_para_modifier.';"'; endif; ?> >
                                          <?php echo $intitule_titre_para; ?>
                                        </h3>
                                        <?php
                                      else:
                                        ?>
                                        <h2 class="police-<?php echo $police;?> justify-<?php echo $alignement; if($uppercase):echo ' to-upper'; endif; if($underline): echo ' underline'; endif;?>"
                                            <?php if($titre_para_modifier): echo 'style="color : '.$couleur_titre_para_modifier.';"'; endif; ?> >
                                          <?php echo $intitule_titre_para; ?>
                                        </h2>
                                        <?php
                                      endif; // ajouter_titre_bloc
                                    endif; // $ajouter_titre_paragraphe

                                    //Contenu du paragraphe :
                                    ?>
                                      <div class="paragraphe-content"
                                        <?php if($texte_para_modifier): echo 'style="color : '.$couleur_texte_para_modifier.'"'; endif; ?>
                                      >
                                      <?php echo $contenu_paragraphe; ?>
                                      </div>
                                    </div>
                                    <?php
                                  endif;// $paragraphe

                                elseif(get_row_layout() == 'image'):

                                    $image_bloc = get_sub_field('image_bloc');
                                    $image = $image_bloc['image'];
                                    if($image):
                                      if($image['sizes']['large']):
                                        $imgUrl = $image['sizes']['large'];
                                      else:
                                        $imgUrl = $image['url'];
                                      endif;

                                      ?>
                                      <div class="image-container" >
                                        <img  src="<?php echo $imgUrl; ?>"
                                              alt="<?php echo $image['alt']; ?>"
                                              <?php if($image['limit_height']): if($image['height']): $height = $image['height']; echo 'style="max-height : '.$height.'px"'; endif;endif; ?> >
                                      </div>
                                      <?php
                                    endif;
                                endif; // get_row_layout
                                ?>
                              </div>
                              <?php
                              $nbContenu++;
                              endwhile; // have_rows('contenu')
                            ?>
                            </div>
                            <?php
                          endif; // have_rows('contenu')

                        endif;// get_row_layout
                        $nb_article ++;
                      endwhile; //have_rows('contenu_article')
                    endif; // have_rows('contenu_article')
                    ?>
                  </div>
                  </div>
                  <!-- End bloc-container  -->
                  <?php

                endwhile; // have_rows('param_bloc')
              endif;//have_rows('param_bloc')

      endwhile; //have_rows('contenu_de_la_page')
    endif; // have_rows('contenu_de_la_page')
    ?>

    </main>
  </div>




<?php get_footer(); ?>
