<?php get_header(); ?>


    <div class="col-12 col-lg-8 col-xl-9 block-page">
      <?php
      $slider1 = get_field('slider_1', 'option');
      $slider2 = get_field('slider_2', 'option');
      $slider3 = get_field('slider_3', 'option');
      ?>

      <ul class="bxslider">
        <li><img src="<?php echo $slider1['sizes']['slider-size'];  ?>"></li>
        <li><img src="<?php echo $slider2['sizes']['slider-size'];  ?>"></li>
        <li><img src="<?php echo $slider3['sizes']['slider-size'];  ?>"></li>
      </ul>

      <div class="action-slider">
        <button type="button" class="locate" data-toggle="modal" data-target="#ModalGmap">
          <i class="far fa-compass"></i>
          <span>Nous situer</span>
        </button>
        <a class="contact" href="/ib_bois/contact/">
          <i class="fas fa-envelope"></i>
          <span>Nous contacter</span>
        </a>
      </div>

      <div class="modal fade" id="ModalGmap" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-body modal-map">
              <?php
              $location = get_field('map', 'option');
              if( !empty($location) ):
              ?>
              <div class="acf-map">
              	<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
              </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>

      <?php if( have_posts() ): while( have_posts() ): the_post(); ?>

          <div class="col-12 block-content">
            <h3><?php the_title(); ?></h3>
            <p><?php the_content(); ?></p>
          </div>

      <?php endwhile; endif; ?>
      <?php get_footer(); ?>

    </div>
