<?php get_header(); ?>


    <div class="col-12 col-lg-8 col-xl-9 block-page">
          <?php if( have_posts() ): while( have_posts() ): the_post(); ?>

              <div class="col-12 block-article-single">
                <img src="<?php the_post_thumbnail_url('large'); ?>" class="post-thumbnail">
                <div class="post-content">
                  <h3><?php the_title(); ?></h3>
                  <small>Posté par <b><?php the_author(); ?></b>, le <?php the_date('d/m/Y'); ?> à <?php the_time('G:i'); ?>.</small>
                  <p><?php the_content(); ?></p>
                </div>
              </div>

          <?php endwhile; endif; ?>
      <?php get_footer(); ?>
    </div>
