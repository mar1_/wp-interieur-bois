<!-- Template Name: Profil -->

<?php get_header(); ?>


  <div class="container block-profile">
    <div class="row">

      <div class="col-12 col-lg-4">

        <?php

        $image = get_field('profile_picture');

        $size = 'custom-size';
      	$thumb = $image['sizes'][ $size ];
      	$width = $image['sizes'][ $size . '-width' ];
      	$height = $image['sizes'][ $size . '-height' ];

        if( !empty($image) ):

        ?>
              <img
              class="profile-picture"
              src="<?php echo $thumb; ?>"
              alt="<?php echo $alt; ?>"
              width="<?php echo $width; ?>"
              height="<?php echo $height; ?>"
              />

        <?php endif; ?>

      </div>

      <div class="col-12 col-lg-8">
        <h2><?php the_field('name'); ?></h2>
        <h4>Profession : <?php the_field('job'); ?></h4>
        <hr>
        <h5>À propos de moi</h5>
        <p><?php the_field('biography') ?></p>
        <hr>
        <h5>Mon projet</h5>
        <p><?php the_field('project') ?></p>
      </div>

    </div>
  </div>


<?php get_footer(); ?>
