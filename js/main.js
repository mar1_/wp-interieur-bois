// Ajoute les classes Bootstrap aux éléments du menu.
// En l'occurrence les éléments 'ul', 'li' et 'a' générés par Wordpress ainsi que la 'div'
// qui les contient.
jQuery(document).ready(function( $ ) {
  var nav_div = document.getElementsByClassName("navbar-collapse");
  var nav_ul = document.getElementById("menu-menu-header");
  var nav_li = document.getElementsByClassName("menu-item");
  var nav_a  = document.querySelectorAll(".menu-item > a");
  $( nav_div ).attr( 'id', 'navbarNav' );
  $( nav_ul ).addClass("navbar-nav");
  $( nav_li ).addClass("nav-item");
  $( nav_a ).addClass("nav-link");
});


// Configuration du BX Slider
// Fonctionnalités consultables sur : bxslider.com/options
jQuery(document).ready(function( $ ) {
  $('.bxslider').bxSlider({
    keyboardEnabled: true,
    controls: false,
    speed: 500,
    auto: true,
    pause: 3000
  }
  );
});


// Preloader
// Attend que la page soit entièrement chargée pour l'afficher et faire disparaître la div
// du preloader.
jQuery(document).ready(function($) {
$(window).load(function(){
	$('#preloader').fadeOut('slow',function(){$(this).remove();});
});

});
